<?php


namespace App\Services;


use App\Serie;
use Illuminate\Support\Facades\DB;

class CriadorSerie
{
    /**
     * @param $nomeSerie
     * @param $numeroTemporadas
     * @param $numeroEpisodios
     * @return Serie
     */
    public function criarSerie($nomeSerie, $numeroTemporadas, $numeroEpisodios, ?string $capa): Serie
    {
        DB::beginTransaction();

        $serie = Serie::create([
            'nome' => $nomeSerie,
            'capa' => $capa
        ]);

        $this->criarTemporadas($numeroTemporadas, $serie, $numeroEpisodios);

        DB::commit();

        return $serie;
    }

    /**
     * @param $numeroTemporadas
     * @param $serie
     * @param $numeroEpisodios
     */
    private function criarTemporadas($numeroTemporadas, $serie, $numeroEpisodios): void
    {
        for ($i = 1; $i <= $numeroTemporadas; $i++) {
            $temporada = $serie->temporadas()->create([
                'numero' => $i,
            ]);

            $this->criarEpisodios($numeroEpisodios, $temporada);
        }
    }

    /**
     * @param $numeroEpisodios
     * @param $temporada
     */
    private function criarEpisodios($numeroEpisodios, $temporada): void
    {
        for ($j = 1; $j <= $numeroEpisodios; $j++) {
            $temporada->episodios()->create([
                'numero' => $j
            ]);
        }
    }
}
