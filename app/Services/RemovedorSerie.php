<?php

namespace App\Services;

use App\Episodio;
use App\Events\RemoverSerie;
use App\Jobs\ExcluirCapaSerie;
use App\Serie;
use App\Temporada;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RemovedorSerie
{
    /**
     * @param int $serieId
     * @return string
     */
    public function removerSerie(int $serieId): string
    {
        $nomeSerie = '';

        DB::transaction(function () use ($serieId, &$nomeSerie) {
            $serie = Serie::find($serieId);
            $serieObj = (object)$serie->toArray();
            $nomeSerie = $serie->nome;

            $this->removerTemporada($serie);
            Serie::destroy($serieId);

//            $eventoRemoverImagemSerie = new RemoverSerie($serieObj);
//            event($eventoRemoverImagemSerie);

            ExcluirCapaSerie::dispatch($serieObj);
        });

        return $nomeSerie;
    }

    /**
     * @param Serie $serie
     */
    private function removerTemporada(Serie $serie): void
    {
        $serie->temporadas->each(function (Temporada $temporada) {
            $this->removerEpisodio($temporada);
            Temporada::destroy($temporada->id);
        });
    }

    /**
     * @param Temporada $temporada
     */
    private function removerEpisodio(Temporada $temporada): void
    {
        $temporada->episodios->each(function (Episodio $episodio) {
            Episodio::destroy($episodio->id);
        });
    }
}
