<?php

namespace App\Providers;

use App\Events\NovaSerie;
use App\Events\RemoverSerie;
use App\Listeners\EnviarEmailSerieCadastrada;
use App\Listeners\LogNovaSerie;
use App\Listeners\RemoverImagemSerie;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NovaSerie::class => [
            EnviarEmailSerieCadastrada::class,
            LogNovaSerie::class
        ]/*,
        RemoverSerie::class => [
            RemoverImagemSerie::class
        ]*/
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
