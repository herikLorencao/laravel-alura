<?php

namespace App\Http\Controllers;


use App\Events\NovaSerie as NovaSerieEvent;
use App\Http\Requests\SeriesRequest;
use App\Mail\NovaSerie;
use App\Serie;
use App\Services\CriadorSerie;
use App\Services\RemovedorSerie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SeriesController extends Controller
{
    public function index(Request $request)
    {
        $series = Serie::query()
            ->orderBy('nome')
            ->get();

        $mensagem = $request->session()->get('mensagem');

        return view('series.index', compact('series', 'mensagem'));
    }

    public function create()
    {
        return view('series.create');
    }

    public function store(SeriesRequest $request, CriadorSerie $criadorSerie)
    {
        $capa = null;

        if ($request->hasFile('capa')) {
            $capa = $request->file('capa')->store('series');
        }

        $serie = $criadorSerie->criarSerie($request->nome, $request->qtd_temporadas, $request->qtd_episodios, $capa);
        $eventoNovaSerie = new NovaSerieEvent($request->nome, $request->qtd_temporadas, $request->qtd_episodios);
        event($eventoNovaSerie);

        $request
            ->session()
            ->flash('mensagem', "Série {$serie->nome} e seus respectivos episódios e temporadas criados");

        return redirect()->route('listar_series');
    }

    /*
     * Uma série pode ser removida em cascata adicionando o método onDelete('cascate') na migration,
     * Na parte de referência da foreign key
     * Obs: Testar se cascadeOnDelete() funciona
     * */
    public function destroy(Request $request, RemovedorSerie $removedorSerie)
    {
        $nomeSerie = $removedorSerie->removerSerie($request->id);

        $request
            ->session()
            ->flash('mensagem', "Série $nomeSerie removida com sucesso");
        return redirect()->route('listar_series');
    }

    public function editaNome(int $id, Request $request)
    {
        $serie = Serie::find($id);
        $serie->nome = $request->nome;
        $serie->save();
    }
}
