<?php

namespace App\Listeners;

use App\Events\NovaSerie;
use App\Mail\NovaSerie as NovaSerieEmail;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class EnviarEmailSerieCadastrada implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NovaSerie $event
     * @return void
     */
    public function handle(NovaSerie $event)
    {
        $users = User::all();

        foreach ($users as $indice => $user) {
            $multiplicador = $indice + 1;

            $email = new NovaSerieEmail($event->nomeSerie, $event->qtdTemporadas, $event->qtdEpisodios);
            $email->subject = "Série {$event->nomeSerie} adicionada";
            $espera = now()->addSeconds($multiplicador * 10);

            Mail::to($user)->later($espera, $email);
        }
    }
}
