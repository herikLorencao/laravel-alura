<?php

namespace App\Listeners;

use App\Events\RemoverSerie;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class RemoverImagemSerie implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RemoverSerie $event
     * @return void
     */
    public function handle(RemoverSerie $event)
    {
        $serie = $event->serie;

        if ($serie->capa) {
            Storage::delete($serie->capa);
        }
    }
}
