@extends('layout')

@section('cabecalho')
    Temporadas de {{ $serie->nome }}
@endsection

@section('conteudo')
    @if($serie->capa)
        <div class="row mb-4 text-center">
            <div class="col-12">
                <a href="{{ $serie->url_imagem }}">
                    <img src="{{ $serie->url_imagem }}" class="img-thumbnail" width="400px" height="400px">
                </a>
            </div>
        </div>
    @endif

    <ul class="list-group">
        @foreach($temporadas as $temporada)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <a href="/temporadas/{{ $temporada->id  }}/episodios">Temporada {{ $temporada->numero }}</a>
                <div class="badge badge-secondary">
                    {{ $temporada->getEpisodiosAssistidos()->count() }} / {{$temporada->episodios()->count()}}
                </div>
            </li>
        @endforeach
    </ul>
@endsection
