@extends('layout')

@section('cabecalho')
    Adicionar Série
@endsection

@section('conteudo')
    @include('erros', ['errors' => $errors])

    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col col-8">
                <label for="nome">Nome</label>
                <input class="form-control" type="text" name="nome" id="nome">
            </div>

            <div class="col col-2">
                <label for="qtd_temporadas">Qtd. Temporadas</label>
                <input class="form-control" type="number" name="qtd_temporadas" id="qtd_temporadas">
            </div>

            <div class="col col-2">
                <label for="qtd_episodios">Qtd. Episódios</label>
                <input class="form-control" type="number" name="qtd_episodios" id="qtd_episodios">
            </div>
        </div>

        <div class="row">
            <label for="capa">Capa</label>
            <input class="form-control" type="file" name="capa" id="capa">
        </div>

        <button class="btn btn-primary mt-3">Adicionar</button>
    </form>
@endsection
